package com.nic.tfw.proxy;

import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

/**
 * Created by Nictogen on 1/10/19.
 */
public class CommonProxy
{
	public void onPreInit(FMLPreInitializationEvent event){

	}
	public void onInit(FMLInitializationEvent event){

	}

	public void stopSounds(EntityPlayer player){

	}

	public void loadShader(EntityLivingBase player, String shader){

	}
}
